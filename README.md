# GoogleAnalytics

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'redwing-analytics'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install redwing-analytics

## Usage

Using this gem is a 2 step process:

1 • Configure you analytics for each environment

Example from voucher app:
```yml
default: &default
  endpoint: "http://www.google-analytics.com/collect"
  version: 1
  data_source: api
  category_prefix: "API - "
  event_category: "Voucher"
  report: true
  server_id: <%=
    #
    # Generate a UUID for this installation, if we haven't already.
    #
    # Will be used as a client_id when no client_id is provided in request
    # we're handling.
    #
    tmpdir = Rails.root.join 'tmp'
    unless Dir.exist?(tmpdir)
      Dir.mkdir tmpdir
    end
    unless File.exist?(Rails.root.join('tmp', '_ga_server_id.uuid'))
      File.write Rails.root.join('tmp', '_ga_server_id.uuid'), SecureRandom.uuid
    end
    File.read Rails.root.join('tmp', '_ga_server_id.uuid')
  %>

beta:
  <<: *default
  category_prefix: "API - Beta - "
  tracking_code: UA-74209007-15

devcon:
  <<: *default
  category_prefix: "API - Development - "
  tracking_code: UA-74209007-14

production:
  <<: *default
  tracking_code: UA-74209007-13

development:
  <<: *default
  report: false

rwbuild:
  <<: *default
  report: false

test:
  <<: *default
  report: false

uat:
  <<: *default
  category_prefix: "API - UAT - "
  tracking_code: UA-74209007-27
```

```ruby
# config/application.rb

class Application < Rails::Application
  # Pulls analytics configuration into application
  config.analytics = config_for(:analytics)
end
```

### Controlling Reporting
Reporting defaults to `true` if no `report` key is present in the yml file for the environment.

The value of the `REPORT_ANALYTICS` environment variable overrides configuration from the yml file.
```bash
REPORT_ANALYTICS=true
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/red-wing/analytics.

