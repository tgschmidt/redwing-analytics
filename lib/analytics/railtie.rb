module Redwing
  module Analytics
    class Railtie < Rails::Railtie
      initializer 'analytics' do
        if report_analytics?
          ::Redwing::Analytics.logger.error { 'Subscribing analytics to controller events' }
          Subscriber.attach_to :action_controller
        end
      end

      def report_analytics?
        return env_report_analytics if ENV.key?('REPORT_ANALYTICS')
        return false unless ::Redwing::Analytics.config.key?('report') # Default to false
        ::Redwing::Analytics.config['report']
      end

      def env_report_analytics
        # Rails 4 helper to turn a string into a boolean
        ActiveRecord::Type::Boolean.new.type_cast_from_user(ENV['REPORT_ANALYTICS'])
      end
    end
  end
end
