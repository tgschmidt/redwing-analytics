require 'rest_client'
require 'active_job'

module Redwing
  module Analytics
    # Job to send events to Analytics
    class ReporterJob < ActiveJob::Base
      rescue_from(RestClient::Exception) do
        retry_job wait: 5.minutes, queue: :low_priority
      end

      def perform(_event_type, params)
        RestClient.get(
          config[:endpoint],
          params: params,
          timeout: 4,
          open_timeout: 4
        )
      end

      private

      def config
        ::Redwing::Analytics.config
      end

      def logger
        ::Redwing::Analytics.logger
      end
    end
  end
end
