require 'logger'

module Redwing
  module Analytics
    class NullLogger < ::Logger # :nodoc:
      def initialize(*args); end

      # Null Logging
      def add(*args, &block); end
    end
  end
end
