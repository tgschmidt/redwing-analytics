require 'active_support'
require 'active_support/subscriber'

module Redwing
  module Analytics
    # Provide request information to GA Job
    class Subscriber < ActiveSupport::Subscriber
      NullEvent = Struct.new(:name) do
        def payload
          @_payload ||= {}
        end
      end

      def process_action(event)
        @event = event || NullEvent.new('null_event')
        ReporterJob.perform_later('event', ga_params)
      rescue => error
        logger.error do
          "Error while enqueuing Analytics: [#{error.message}]"
        end
        false
      end

      # Build a hash in the shape that GA expects.
      def ga_params
        {
          v: config[:version],          # API version
          ds: config[:data_source],     # API data source
          tid: config[:tracking_code],  # tracking ID
          cid: '999',                   # client ID
          t: 'event',                   # hit type
          ec: config[:event_category],  # event category
          ea: payload.fetch(:path, ''), # event action
          ev: request_time * 1000       # event value
        }
      end

      # Get the total request time the same way the standard Rails logger does.
      def request_time
        view_runtime.ceil + db_runtime.ceil
      end

      private

      def view_runtime
        # Looks for :view_runtime, ensures that nil does not leak through
        payload.fetch(:view_runtime, false) || 0
      end

      def db_runtime
        # Looks for :db_runtime, ensures that nil does not leak through
        payload.fetch(:db_runtime, false) || 0
      end

      def config
        ::Redwing::Analytics.config
      end

      def logger
        ::Redwing::Analytics.logger
      end

      def payload
        @event.try(:payload) || {}
      end
    end
  end
end
