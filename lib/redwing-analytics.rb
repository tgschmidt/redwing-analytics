if caller[0] !~ /\/bundler\/runtime\.rb:\d+:in `require'/
    warn "Don't require 'redwing-analytics'. Use \"require 'analytics'\" instead. 'redwing-analytics.rb' was added only for 'Bundler.require'."
end
require 'analytics'
