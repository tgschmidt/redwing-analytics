require 'analytics/version'
require 'analytics/subscriber'
require 'analytics/reporter_job'
require 'analytics/null_logger'
require 'analytics/railtie' if defined?(Rails)

require 'active_support'

module Redwing # :nodoc:
  module Analytics # :nodoc:
    class << self

      def logger
        @logger ||= begin
                      return NullLogger.new unless defined?(::Rails)
                      ::Rails.logger
                    end
      end

      def config
        @config ||= begin
                      return railtie_config if defined?(Railtie)
                      raise('Missing analytics.yml configuration.')
                    end
      end

      private

      def railtie_config
        ActiveSupport::HashWithIndifferentAccess.new(Railtie.config.analytics)
      end
    end
  end
end
