# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'analytics/version'

Gem::Specification.new do |spec|
  spec.name          = "redwing-analytics"
  spec.version       = Redwing::Analytics::VERSION
  spec.authors       = ["Chad Metcalf"]
  spec.email         = ["cmetcalf@multiservice.com"]

  spec.summary       = %q{Analytics for multiple environments}
  spec.homepage      = 'https://scm.multiservice.com/gitlab/red-wing/analytics'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "https://repo.multiservice.com"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency 'activesupport', '>=4.0'
  spec.add_dependency 'activejob', '>=4.0'
  spec.add_dependency 'railties', '>=4.0'
  spec.add_dependency "rest-client"

  spec.add_development_dependency "bundler", "~> 1.13"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "minitest", "~> 5.0"
  spec.add_development_dependency "mocha"
end
