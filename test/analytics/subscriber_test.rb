require 'pry'
require 'test_helper'

module Redwing
  module Analytics
    class SubscriberTest < Minitest::Test
      def setup
        @subscriber = Subscriber.new

        @event = mock('event')
        @event.stubs(:payload).returns(db_runtime: 1, view_runtime: 1)
      end

      def test_enqueues_reporter_worker
        ReporterJob.stubs(:perform_later).returns(true)

        assert @subscriber.process_action(@event)
      end

      def test_catches_enqueueing_errors
        ReporterJob.stubs(:perform_later)
                   .raises(StandardError, 'Analytics Reporting Job Error')

        refute @subscriber.process_action(@event)
      end

      def test_handles_nil_and_missing_response_times
        # missing :db_runtime key
        @subscriber.stubs(:payload).returns(view_runtime: nil)

        assert_equal 0, @subscriber.request_time
      end
    end
  end
end
