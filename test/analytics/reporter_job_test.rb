require 'test_helper'

module Redwing
  module Analytics
    class ReporterJobTest < ActiveSupport::TestCase
      test 'pings google' do
        RestClient.expects(:get).returns(true)

        job = ReporterJob.new
        job.perform('event', {})
      end
    end
  end
end
