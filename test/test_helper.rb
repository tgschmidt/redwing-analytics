$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)

require 'analytics'

require 'minitest/autorun'
require 'mocha/mini_test'
require 'ostruct'

module Redwing
  module Analytics
    class Railtie
      def self.config
        @config ||= OpenStruct.new(analytics: {})
      end
    end
  end
end
