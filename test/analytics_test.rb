require 'test_helper'

module Redwing
  module Analytics
    class AnalyticsTest
      def test_that_it_has_a_version_number
        refute_nil ::Analytics::VERSION
      end
    end
  end
end
